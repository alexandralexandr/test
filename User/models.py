from django.contrib.auth.models import AbstractUser

from Cart.cart import WishList, ShoppingCart


class User(AbstractUser):

    @property
    def wish_list(self):
        return WishList(self)

    @property
    def shopping_cart(self):
        return ShoppingCart(self)
