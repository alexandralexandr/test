from http import HTTPStatus

from rest_framework.permissions import AllowAny
from rest_framework.response import Response

from django.contrib.auth import login, authenticate
from rest_framework.viewsets import ViewSet

from User.serializers import AuthSerializer


class AuthViewSet(ViewSet):
    permission_classes = [AllowAny]

    def post(self, request):
        serializer = AuthSerializer(data=request.data)
        if serializer.is_valid():
            user = authenticate(**serializer.validated_data)
            if not user:
                return Response(data={'errors': {'username': 'Username or password is incorrect'}},
                                status=HTTPStatus.BAD_REQUEST)
            elif user.is_active:
                login(request, user)
                request.session.set_expiry(60 * 60 * 24 * 7)
                return Response(status=HTTPStatus.OK)

            return Response(data={'errors': {'username': 'Username is not active'}},
                            status=HTTPStatus.BAD_REQUEST)

        return Response(data={'errors': serializer.errors}, status=HTTPStatus.BAD_REQUEST)
