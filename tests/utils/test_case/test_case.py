from django.test import TestCase
from rest_framework.test import APIClient, APIRequestFactory

from User.models import User


class BaseTestCase(TestCase):
    username = 'username'
    password = 'password1234a'

    def assertQsContainsItem(self, qs, *args):
        [self.assertTrue(qs.filter(pk=item.id).exists()) for item in args]

    def assertQsNotContainsItem(self, qs, *args):
        [self.assertFalse(qs.filter(pk=item.id).exists()) for item in args]

    def setUp(self):
        super().setUp()
        self.user, created = User.objects.get_or_create(username=self.username, is_active=True)
        self.user.set_password(self.password)
        self.user.save()


class ApiTestCase(BaseTestCase):
    authenticate = True

    def setUp(self):
        super().setUp()
        if self.authenticate:
            self.client = APIClient()
            self.client.login(username=self.username, password=self.password)
            self.request = APIRequestFactory(user=self.user)
            self.request.user = self.user
