from http import HTTPStatus

from django.db.models import Max
from django.urls import reverse

from Product.models import Product
from tests.utils.test_case.test_case import ApiTestCase


class BaseCartTestCase(ApiTestCase):
    fixtures = ['initial_data.json']
    url = ''

    def setUp(self):
        super().setUp()
        self.product = Product.objects.first()

    @property
    def _not_existed_pk(self):
        return Product.objects.aggregate(Max('id'))['id__max'] + 1

    def _check_contains_product(self):
        response = self.client.get(reverse(self.url))
        self.assertContains(response, self.product.name)

    def _check_not_contains_product(self):
        response = self.client.get(reverse('api-wish-list-list'))
        self.assertNotContains(response, self.product.name)


class WishListApiTests(BaseCartTestCase):
    url = 'api-wish-list-list'

    def setUp(self):
        super().setUp()
        self.user.wish_list.clear()
        self.user.shopping_cart.clear()

    def test_list(self):
        self.user.wish_list.add(self.product)
        self._check_contains_product()

    def test_successful_add_to_shopping_cart(self):
        response = self.client.post(reverse(self.url), data={'product': self.product.id})
        self.assertEqual(response.status_code, HTTPStatus.CREATED)
        self._check_contains_product()

    def test_fail_add_to_wish_list(self):
        data = {
            'product': self._not_existed_pk
        }
        response = self.client.post(reverse(self.url), data=data)
        self.assertEqual(response.status_code, HTTPStatus.BAD_REQUEST)
        self._check_not_contains_product()

    def test_successful_delete_from_wish_list(self):
        self.user.wish_list.add(self.product)
        response = self.client.delete(reverse(self.url), data={'product': self.product.id})
        self.assertEqual(response.status_code, HTTPStatus.OK)
        self._check_not_contains_product()

    def test_fail_delete_from_wish_list(self):
        data = {
            'product': self._not_existed_pk
        }
        self.user.wish_list.add(self.product)
        response = self.client.delete(reverse(self.url), data=data)
        self.assertEqual(response.status_code, HTTPStatus.BAD_REQUEST)

        self._check_contains_product()

    def test_successful_clear(self):
        self.user.wish_list.add(self.product)
        response = self.client.delete(reverse(self.url))
        self.assertEqual(response.status_code, HTTPStatus.OK)
        self._check_not_contains_product()

    def test_successful_move_to_shopping_cart(self):
        self.user.wish_list.add(self.product)
        quantity = 10

        response = self.client.post(reverse('api-wish-list-move-to-shopping-cart'), data={'product': self.product.id,
                                                                                          'quantity': quantity})
        self.assertEqual(response.status_code, HTTPStatus.OK)

        self._check_not_contains_product()
        self.assertTrue((self.product, quantity) in self.user.shopping_cart.list())


class ShoppingCartApiTests(BaseCartTestCase):
    url = 'api-shopping-cart-list'
    quantity = 10

    def test_list(self):
        self.user.shopping_cart.add(self.product, self.quantity)
        self._check_contains_product()

    def test_successful_add_to_shopping_cart(self):
        response = self.client.post(reverse(self.url), data={'product': self.product.id,
                                                             'quantity': self.quantity})
        self.assertEqual(response.status_code, HTTPStatus.CREATED)
        self._check_contains_product()

    def test_fail_add_to_wish_list(self):
        product = Product.objects.last()
        data = [
            {
                'product': self._not_existed_pk,  # invalid pk
                'quantity': self.quantity
            },
            {
                'product': product.id,
                'quantity': -1  # invalid quantity
            }
        ]
        for _data in data:
            response = self.client.post(reverse(self.url), data=_data)
            self.assertEqual(response.status_code, HTTPStatus.BAD_REQUEST)

            response = self.client.get(reverse('api-wish-list-list'))
            self.assertNotContains(response, product.name)

    def test_successful_change_quantity_shopping_cart(self):
        response = self.client.post(reverse(self.url), data={'product': self.product.id,
                                                             'quantity': self.quantity})
        self.assertEqual(response.status_code, HTTPStatus.CREATED)
        self._check_contains_product()

    def test_fail__change_quantity_shopping_cart(self):
        data = [
            {
                'product': self._not_existed_pk,  # invalid pk
                'quantity': self.quantity
            },
            {
                'product': self.product.pk,
                'quantity': -1  # invalid quantity
            }
        ]
        for _data in data:
            response = self.client.post(reverse(self.url), data=_data)
            self.assertEqual(response.status_code, HTTPStatus.BAD_REQUEST)
            self._check_not_contains_product()

    def test_successful_delete_from_wish_list(self):
        self.user.shopping_cart.add(self.product, self.quantity)
        response = self.client.delete(reverse(self.url), data={'product': self.product.id})
        self.assertEqual(response.status_code, HTTPStatus.OK)
        self._check_not_contains_product()

    def test_fail_delete_from_shopping_cart(self):
        data = {
            'product': self._not_existed_pk
        }
        self.user.shopping_cart.add(self.product, self.quantity)
        response = self.client.delete(reverse(self.url), data=data)
        self.assertEqual(response.status_code, HTTPStatus.BAD_REQUEST)

        self._check_contains_product()

    def test_successful_clear(self):
        self.user.shopping_cart.add(self.product, self.quantity)
        response = self.client.delete(reverse(self.url))
        self.assertEqual(response.status_code, HTTPStatus.OK)
        self._check_not_contains_product()
