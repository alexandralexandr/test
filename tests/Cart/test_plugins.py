from importlib import reload

from django.test import override_settings

from Product.models import Product
from tests.utils.test_case.test_case import BaseTestCase


def _reload():
    # dirty hack need reload modules to override cart plugin during tests
    import User.models
    import Cart.cart
    reload(Cart.cart)
    reload(User.models)


class BasePluginTestCase(BaseTestCase):

    fixtures = ['initial_data.json']

    def setUp(self):
        super().setUp()
        self.product1 = Product.objects.first()
        self.product2 = Product.objects.last()

    def run(self, *args, **kwargs):
        # call run for each card plugins
        for plugin in ['db', 'redis']:
            with override_settings(CART_PLUGIN=plugin):
                _reload()
                super().run(*args, **kwargs)


class PluginWishListTests(BasePluginTestCase):

    def setUp(self):
        super().setUp()
        self.wish_list = self.user.wish_list
        self.wish_list.clear()

    def test_add_product(self):
        wish_list = self.wish_list
        product1 = Product.objects.first()
        product2 = Product.objects.last()

        wish_list.add(product1)
        qs = wish_list.list()

        self.assertQsContainsItem(qs, product1)
        self.assertQsNotContainsItem(qs, product2)

    def test_remove_product(self):
        wish_list = self.wish_list

        wish_list.add(self.product1)
        wish_list.add(self.product2)

        wish_list.remove(self.product1)

        qs = wish_list.list()

        self.assertQsNotContainsItem(qs, self.product1)
        self.assertQsContainsItem(qs, self.product2)

    def test_clear(self):
        wish_list = self.wish_list
        product1 = Product.objects.first()
        product2 = Product.objects.last()

        wish_list.add(product1)
        wish_list.add(product2)

        wish_list.clear()

        self.assertFalse(wish_list.list())

    def test_move_to_shopping_cart(self):
        wish_list = self.wish_list
        shopping_cart = self.user.shopping_cart
        shopping_cart.clear()

        product1 = self.product1
        quantity = 10

        wish_list.add(product1)
        wish_list.move_to_shopping_cart(product1, 10)

        self.assertFalse(wish_list.list())
        data = shopping_cart.list()
        self.assertTrue((self.product1, quantity) in data)


class PluginShoppingCartTests(BasePluginTestCase):

    def setUp(self):
        super().setUp()
        self.shopping_cart = self.user.shopping_cart
        self.shopping_cart.clear()

    def test_add_product(self):
        shopping_cart = self.shopping_cart

        quantity1 = 10
        shopping_cart.add(self.product1, quantity1)
        quantity2 = 20
        shopping_cart.add(self.product2, quantity2)

        data = shopping_cart.list()
        self.assertTrue((self.product1, quantity1) in data)
        self.assertTrue((self.product2, quantity2) in data)

    def test_remove_product(self):
        shopping_cart = self.shopping_cart

        quantity1 = 10
        shopping_cart.add(self.product1, quantity1)
        quantity2 = 20
        shopping_cart.add(self.product2, quantity2)

        data = shopping_cart.list()
        self.assertTrue((self.product1, quantity1) in data)
        self.assertTrue((self.product2, quantity2) in data)

        shopping_cart.remove(self.product1)

        data = shopping_cart.list()
        self.assertFalse((self.product1, quantity1) in data)
        self.assertTrue((self.product2, quantity2) in data)

    def test_change_quantity(self):
        shopping_cart = self.shopping_cart

        quantity1 = 10
        quantity2 = 20

        shopping_cart.add(self.product1, quantity1)
        data = shopping_cart.list()
        self.assertTrue((self.product1, quantity1) in data)
        self.assertFalse((self.product1, quantity2) in data)

        shopping_cart.change_quantity(self.product1, quantity2)
        data = shopping_cart.list()
        self.assertTrue((self.product1, quantity2) in data)
        self.assertFalse((self.product1, quantity1) in data)

    def test_amount(self):
        shopping_cart = self.shopping_cart

        quantity1 = 10
        quantity2 = 20

        shopping_cart.add(self.product1, quantity1)
        shopping_cart.add(self.product2, quantity2)

        amount = self.product1.price * quantity1 + self.product2.price * quantity2

        self.assertEqual(self.shopping_cart.amount(), amount)
