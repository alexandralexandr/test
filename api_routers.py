from rest_framework import routers

from django.conf.urls import include, url

from Cart.api import CardViewSet, ShoppingCartViewSet
from User.api import AuthViewSet

router = routers.DefaultRouter()
router.register(r'auth', AuthViewSet, base_name='api-auth')
router.register(r'wish-list', CardViewSet, base_name='api-wish-list')
router.register(r'shopping-cart', ShoppingCartViewSet, base_name='api-shopping-cart')


urlpatterns = [
    url(r'^', include(router.urls), name='api-root'),
]
