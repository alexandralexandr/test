from http import HTTPStatus

from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.viewsets import ViewSet

from Cart.serializers import WishListSerializer, ShoppingCartSerializer, ShoppingCartAddSerializer, \
    RemoveFromCartSerializer
from Product.serializers import ProductSerializer


class CardViewSet(ViewSet):
    permission_classes = [IsAuthenticated]

    def list(self, request):
        return Response(data=ProductSerializer(instance=request.user.wish_list.list(),  many=True).data)

    def create(self, request):
        serializer = WishListSerializer(data=request.data)
        if serializer.is_valid():
            request.user.wish_list.add(serializer.validated_data['product'])
            return Response(status=HTTPStatus.CREATED)

        return Response(data={'errors': serializer.errors}, status=HTTPStatus.BAD_REQUEST)

    def delete(self, request):
        # if request.data - remove given product
        # if request.data empty - clear wish list
        if request.data:
            serializer = RemoveFromCartSerializer(data=request.data)
            if serializer.is_valid():
                request.user.wish_list.remove(serializer.validated_data['product'])
                return Response(status=HTTPStatus.OK)
            return Response(data={'errors': serializer.errors}, status=HTTPStatus.BAD_REQUEST)
        else:
            request.user.wish_list.clear()
            return Response(status=HTTPStatus.OK)

    @action(methods=['POST'], detail=False, name='move-to-shopping-cart', url_path='move-to-shopping-cart')
    def move_to_shopping_cart(self, request):
        serializer = ShoppingCartAddSerializer(data=request.data)
        if serializer.is_valid():

            request.user.wish_list.move_to_shopping_cart(**serializer.validated_data)
            return Response(status=HTTPStatus.OK)
        return Response(data={'errors': serializer.errors}, status=HTTPStatus.BAD_REQUEST)


class ShoppingCartViewSet(ViewSet):

    def list(self, request):
        data = [{'product': product, 'quantity': quantity} for product, quantity in request.user.shopping_cart.list()]
        return Response(data=ShoppingCartSerializer(instance=data, many=True).data)

    def create(self, request):
        serializer = ShoppingCartAddSerializer(data=request.data)
        if serializer.is_valid():
            request.user.shopping_cart.add(**serializer.validated_data)
            return Response(status=HTTPStatus.CREATED)

        return Response(data={'errors': serializer.errors}, status=HTTPStatus.BAD_REQUEST)

    def update(self, request):
        serializer = ShoppingCartAddSerializer(data=request.data)
        if serializer.is_valid():
            request.user.shopping_cart.addchange_quantity(**serializer.validated_data)
            return Response(status=HTTPStatus.CREATED)

        return Response(data={'errors': serializer.errors}, status=HTTPStatus.BAD_REQUEST)

    def delete(self, request):
        if request.data:
            serializer = RemoveFromCartSerializer(data=request.data)
            if serializer.is_valid():
                request.user.shopping_cart.remove(serializer.validated_data['product'])
                return Response(status=HTTPStatus.OK)
            return Response(data={'errors': serializer.errors}, status=HTTPStatus.BAD_REQUEST)
        else:
            request.user.shopping_cart.clear()
            return Response(status=HTTPStatus.OK)
