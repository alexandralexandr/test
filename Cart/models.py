from django.db import models

from Product.models import Product


class BaseCartModel(models.Model):
    user = models.ForeignKey('User.User', on_delete=models.CASCADE)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)

    class Meta:
        abstract = True
        unique_together = ('user', 'product')


class WishList(BaseCartModel):

    class Meta:
        db_table = 'wish_list'


class ShoppingCart(BaseCartModel):
    # TODO: can be Decimal
    quantity = models.PositiveIntegerField()

    class Meta:
        db_table = 'shopping_cart'
