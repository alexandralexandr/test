from importlib import import_module

from django.conf import settings

_module = import_module(f'Cart.plugins.{settings.CART_PLUGIN}')
ShoppingCart, WishList = getattr(_module, 'ShoppingCart'), getattr(_module, 'WishList')
