from django.db.models import Sum, DecimalField
from django.db.models.expressions import F

from Cart.plugins.base import BaseShoppingCart, BaseWishList

from Cart.models import WishList as WishListModel, ShoppingCart as ShoppingCartModel
from Product.models import Product


class WishList(BaseWishList):

    @property
    def _default_qs(self):
        return WishListModel.objects.filter(user=self.user)

    def add(self, product):
        self._default_qs.get_or_create(user=self.user, product=product)

    def clear(self):
        self._default_qs.delete()

    def list(self):
        return Product.objects.filter(pk__in=self._default_qs.values_list('product', flat=True))

    def remove(self, product):
        self._default_qs.filter(user=self.user, product=product).delete()


class ShoppingCart(BaseShoppingCart):

    @property
    def _default_qs(self):
        return ShoppingCartModel.objects.filter(user=self.user)

    def add(self, product, quantity):
        qs = self._default_qs.filter(product=product)
        qs_params = {'user': self.user, 'product': product}
        if qs.exists():
            ShoppingCartModel.objects.update(defaults={'quantity': F('quantity') + quantity}, **qs_params)
        else:
            ShoppingCartModel.objects.create(quantity=quantity, **qs_params)

    def clear(self):
        self._default_qs.delete()

    def list(self):
        return [(item.product, item.quantity) for item in self._default_qs.select_related(
            'product').only('product', 'quantity')]

    def remove(self, product):
        self._default_qs.filter(product=product).delete()

    def change_quantity(self, product, quantity):
        cart, created = ShoppingCartModel.objects.get_or_create(user=self.user, product=product, defaults={
            'quantity': quantity
        })
        if not created:
            cart.quantity = quantity
            cart.save()

    def amount(self):
        return self._default_qs.aggregate(amount=Sum(F('product__price') * F('quantity'), output_field=DecimalField()),
                                          )['amount']
