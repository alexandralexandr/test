from redis import Redis

from Cart.plugins.base import BaseShoppingCart, BaseWishList
from Product.models import Product


class BaseRedisCart:
    _key_format = '_%s'
    _connection = None

    # def __del__(self):
    #     if self._connection:
    #         self._connection.close()

    @property
    def key(self):
        return self._key_format % self.user.id

    @property
    def connection(self):
        if not self._connection:
            self._connection = Redis()
        return self._connection

    def clear(self):
        self.connection.delete(self.key)


class WishList(BaseRedisCart, BaseWishList):

    def __str__(self):
        return 'Redis wish list for user %s '

    def add(self, product):
        self.connection.sadd(self.key, product.id)

    def list(self):
        pk_list = self.connection.smembers(self.key)
        if not pk_list:
            return Product.objects.none()
        return Product.objects.filter(id__in=pk_list)

    def remove(self, product):
        self.connection.srem(self.key, product.id)


class ShoppingCart(BaseRedisCart, BaseShoppingCart):
    _key_format = 'shopping_cart_%s'

    def __str__(self):
        return 'Redis stored card for user %s '

    def _current_value(self, product):
        return int(self.connection.hmget(self.key, product.id)[0] or 0)

    def add(self, product, quantity):
        """
        :param product:
        :param quantity:
        :return:
        set if current is empty. Update if already exits
        """
        # TODO: maybe raise exception if already exits
        self.connection.hmset(self.key, {product.id: self._current_value(product) + quantity})

    def list(self):
        items = self.connection.hgetall(self.key)
        if not items:
            return []
        pk_list = items.keys()
        return list(zip(Product.objects.filter(id__in=pk_list).order_by('id'),
                        sorted(map(int, items.values()))))

    def remove(self, product):
        self.connection.hdel(self.key, product.id)

    def change_quantity(self, product, quantity):
        self.connection.hmset(self.key, {product.id: quantity})

    def amount(self):
        return sum([product.price * quantity for product, quantity in self.list()])
