class BaseCart:

    def __init__(self, user):
        self.user = user

    def list(self):
        raise NotImplementedError

    def remove(self, product):
        raise NotImplementedError


class BaseWishList(BaseCart):

    def move_to_shopping_cart(self, product, quantity):
        # TODO: maybe raise error if product not exists in wish list
        self.user.shopping_cart.add(product, quantity)
        self.remove(product)

    def add(self, product):
        raise NotImplementedError

    def clear(self):
        raise NotImplementedError

    def list(self):
        raise NotImplementedError

    def remove(self, product):
        raise NotImplementedError


class BaseShoppingCart(BaseCart):

    def add(self, product, quantity):
        raise NotImplementedError

    def change_quantity(self, product, quantity):
        # TODO: maybe check item not exists in cart
        raise NotImplementedError

    @property
    def amount(self):
        raise NotImplementedError
