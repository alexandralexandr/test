from rest_framework import serializers

from Product.models import Product
from Product.serializers import ProductSerializer


class WishListSerializer(serializers.Serializer):
    product = serializers.PrimaryKeyRelatedField(queryset=Product.objects.all())


class RemoveFromCartSerializer(serializers.Serializer):
    product = serializers.PrimaryKeyRelatedField(queryset=Product.objects.all())


class ShoppingCartAddSerializer(serializers.Serializer):
    product = serializers.PrimaryKeyRelatedField(queryset=Product.objects.all())
    quantity = serializers.IntegerField(min_value=1)


class ShoppingCartSerializer(serializers.Serializer):
    product = ProductSerializer()
    quantity = serializers.IntegerField(min_value=1)
