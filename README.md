# test project

--------

# Local development

* Install `docker` and `docker-compose` using [following instructions](https://docs.docker.com/compose/install/)
* run: 
```docker-compose up```

## Local installation:
* clone source code
* create `virualenv`
* run `pip instlal .`
* run `docker-compose up`
* run `docker exec -it test_db_1 psql -U postgres -c 'create database test'`
* run `python manage.py migrate` 
* run `python manage.py loaddata initial_data.json`


## Data migration:
 * run `python manage.py migrate`

# Testing

* install testing requirements:
`pip install -r test_requirements.txt`
`python manage.py test`

# Run
 - `manage.py runserver`
 

# Troubleshooting

* 