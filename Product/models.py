from django.db import models


class Product(models.Model):
    name = models.CharField(max_length=128)
    price = models.DecimalField(decimal_places=2, max_digits=8)
    image = models.URLField(max_length=256)

    class Meta:
        db_table = 'product'
